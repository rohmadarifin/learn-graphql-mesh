// @ts-nocheck
import { GraphQLResolveInfo, SelectionSetNode, FieldNode, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  JSON: any;
  _Any: any;
};

export type Query = {
  /**
   * Index Page
   *
   * Equivalent to GET /
   */
  auditLogIndexResponse?: Maybe<AuditLogIndexResponse>;
  /**
   * Get supply canvassing
   *
   * Equivalent to GET /supply
   */
  canvassingArrayResponse?: Maybe<CanvassingArrayResponse>;
  /**
   * Update a new supply canvassing
   *
   * Equivalent to GET /supply/{supply_id}
   */
  canvassingSingleResponse?: Maybe<CanvassingSingleResponse>;
  /**
   * Get supply lead in supply canvassing
   *
   * Equivalent to GET /supply/summary
   */
  canvassingSummaryResponse?: Maybe<CanvassingSummaryResponse>;
  countries: Array<Country>;
  country?: Maybe<Country>;
  continents: Array<Continent>;
  continent?: Maybe<Continent>;
  languages: Array<Language>;
  language?: Maybe<Language>;
  _entities: Array<Maybe<_Entity>>;
};


export type QuerycanvassingArrayResponseArgs = {
  keyword?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QuerycanvassingSingleResponseArgs = {
  supplyId: Scalars['String'];
};


export type QuerycanvassingSummaryResponseArgs = {
  dateEnd?: InputMaybe<Scalars['String']>;
  dateStart?: InputMaybe<Scalars['String']>;
};


export type QuerycountriesArgs = {
  filter?: InputMaybe<CountryFilterInput>;
};


export type QuerycountryArgs = {
  code: Scalars['ID'];
};


export type QuerycontinentsArgs = {
  filter?: InputMaybe<ContinentFilterInput>;
};


export type QuerycontinentArgs = {
  code: Scalars['ID'];
};


export type QuerylanguagesArgs = {
  filter?: InputMaybe<LanguageFilterInput>;
};


export type QuerylanguageArgs = {
  code: Scalars['ID'];
};


export type Query_entitiesArgs = {
  representations: Array<Scalars['_Any']>;
};

export type Mutation = {
  /**
   * Get Logs by Filter. Request body must have at least 1 key-value
   *
   * Equivalent to POST /search
   */
  searchAuditlogs?: Maybe<Search>;
  /**
   * Store audit log to database
   *
   * Equivalent to POST /log
   */
  storeAuditlog?: Maybe<Log>;
  /**
   * Create a new supply canvassing
   *
   * Equivalent to POST /supply
   */
  createSupply?: Maybe<CanvassingSingleResponse>;
  /**
   * Get supply lead in supply canvassing
   *
   * Equivalent to POST /supply/lead
   */
  getSupplyLead?: Maybe<Array<Maybe<CanvassingSingleResponse>>>;
  /**
   * Update a new supply canvassing
   *
   * Equivalent to POST /supply/{supply_id}
   */
  updateSupplyById?: Maybe<CanvassingSingleResponse>;
};


export type MutationsearchAuditlogsArgs = {
  getLogsByFilterInput: GetLogsByFilterInput;
};


export type MutationstoreAuditlogArgs = {
  getLogsByFilterInput: GetLogsByFilterInput;
};


export type MutationcreateSupplyArgs = {
  canvassingRequestInput: CanvassingRequestInput;
};


export type MutationgetSupplyLeadArgs = {
  getSupplyByLeadRequestInput: GetSupplyByLeadRequestInput;
  limit?: InputMaybe<Scalars['Int']>;
};


export type MutationupdateSupplyByIdArgs = {
  canvassingRequestInput: CanvassingRequestInput;
  supplyId: Scalars['String'];
};

export type AuditLogIndexResponse = {
  data?: Maybe<Scalars['JSON']>;
  message: Scalars['String'];
  status: Scalars['String'];
};

export type Search = {
  data?: Maybe<Array<Maybe<AuditSearchLogsResponse>>>;
  message?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type AuditSearchLogsResponse = {
  activity?: Maybe<Scalars['String']>;
  actor?: Maybe<Scalars['String']>;
  clientIdRef?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  project?: Maybe<Scalars['String']>;
  sysData?: Maybe<Scalars['String']>;
  sysMessage?: Maybe<Scalars['String']>;
  sysStatus?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

export type GetLogsByFilterInput = {
  activity: Scalars['String'];
  actor?: InputMaybe<Scalars['String']>;
  clientIdRef: Scalars['String'];
  content: Scalars['String'];
  project: Scalars['String'];
  sysData?: InputMaybe<Scalars['String']>;
  sysMessage?: InputMaybe<Scalars['String']>;
  sysStatus?: InputMaybe<Scalars['String']>;
  tag?: InputMaybe<Scalars['String']>;
  type: Scalars['String'];
};

export type Log = {
  data?: Maybe<Array<Maybe<AuditSearchLogsResponse>>>;
  message?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type CanvassingArrayResponse = {
  data: Array<Maybe<CanvassingResponse>>;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingResponse = {
  commoditySize?: Maybe<Scalars['Float']>;
  commoditySizeMax?: Maybe<Scalars['Float']>;
  cultivationSystem?: Maybe<CultivationSystem>;
  feedConsumptionKg?: Maybe<Scalars['Float']>;
  feedType?: Maybe<Scalars['String']>;
  harvestDateEnd?: Maybe<Scalars['String']>;
  harvestDateStart?: Maybe<Scalars['String']>;
  harvestTonnageEstimationKg?: Maybe<Scalars['Float']>;
  harvestType?: Maybe<HarvestType>;
  hasHarvestTeam?: Maybe<Scalars['Boolean']>;
  hasLogistic?: Maybe<Scalars['Boolean']>;
  hasSortingTeam?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
  isInterest?: Maybe<Scalars['Boolean']>;
  notes?: Maybe<Scalars['String']>;
  pondAddress?: Maybe<Scalars['String']>;
  pondCityId?: Maybe<Scalars['Float']>;
  pondCityName?: Maybe<Scalars['String']>;
  pondConstruction?: Maybe<PondConstruction>;
  pondName?: Maybe<Scalars['String']>;
  pondProvinceId?: Maybe<Scalars['Float']>;
  pondProvinceName?: Maybe<Scalars['String']>;
  pondSubdistrictId?: Maybe<Scalars['Float']>;
  pondSubdistrictName?: Maybe<Scalars['String']>;
  priceOnCar?: Maybe<Scalars['Int']>;
  priceOnFarm?: Maybe<Scalars['Int']>;
  priceOnFarmMax?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['String']>;
  products?: Maybe<CanvassingProduct>;
  sampling?: Maybe<Array<Maybe<SamplingListItem>>>;
  supplierId?: Maybe<Scalars['String']>;
  supplierName?: Maybe<Scalars['String']>;
  supplierTelp?: Maybe<Scalars['String']>;
  supplyCategory?: Maybe<SupplyCategory>;
  supplyType?: Maybe<SupplyType>;
  unitCommoditySize?: Maybe<UnitCommoditySize>;
};

export type CultivationSystem =
  | 'INTENSIVE'
  | 'SEMI_INTENSIVE'
  | 'TRADISIONAL';

export type HarvestType =
  | 'TOTAL'
  | 'PARTIAL';

export type PondConstruction =
  | 'BETON'
  | 'MULSA'
  | 'HDPE'
  | 'TANAH'
  | 'TERPAL';

export type CanvassingProduct = {
  commodityCategory?: Maybe<CommodityCategory>;
  commodityCategoryId?: Maybe<Scalars['String']>;
  commoditySize?: Maybe<Scalars['Float']>;
  commoditySizeMax?: Maybe<Scalars['Float']>;
  createdAt?: Maybe<Scalars['String']>;
  glazing?: Maybe<Scalars['String']>;
  grade?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  isCanvassing?: Maybe<Scalars['Boolean']>;
  packingSize?: Maybe<Scalars['String']>;
  productType?: Maybe<Scalars['String']>;
  remark?: Maybe<Scalars['String']>;
  skuName?: Maybe<Scalars['String']>;
  unitCommoditySize?: Maybe<Scalars['String']>;
  unitInventory?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
};

export type CommodityCategory = {
  category?: Maybe<Scalars['String']>;
  commodityName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type SamplingListItem = {
  docVisit?: Maybe<Scalars['Float']>;
  meanBodyWeight?: Maybe<Scalars['Float']>;
  notes?: Maybe<Scalars['String']>;
  quality?: Maybe<Scalars['String']>;
  tonnageEstimationKg?: Maybe<Scalars['Float']>;
  waterQuality?: Maybe<Scalars['String']>;
};

export type SupplyCategory =
  | 'KOLAM'
  | 'BATCH_PANEN';

export type SupplyType =
  | 'FISH'
  | 'SHRIMP';

export type UnitCommoditySize =
  | 'GRAM_EKOR'
  | 'EKOR_KG'
  | 'GRAM_PCS'
  | 'PCS_PAK';

export type CanvassingSingleResponse = {
  data: CanvassingResponse;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingSummaryResponse = {
  data: CanvassingSummary;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingSummary = {
  totalSupply: TotalSupply;
  totalValid: TotalValid;
};

export type TotalSupply = {
  totalHarvest: Scalars['Float'];
  totalPrice: Scalars['Float'];
};

export type TotalValid = {
  totalHarvest: Scalars['Float'];
  totalPrice: Scalars['Float'];
};

export type CanvassingRequestInput = {
  commoditySize: Scalars['Float'];
  commoditySizeMax?: InputMaybe<Scalars['Float']>;
  cultivationSystem?: InputMaybe<CultivationSystem>;
  feedConsumptionKg: Scalars['Float'];
  feedType?: InputMaybe<Scalars['String']>;
  harvestDateEnd?: InputMaybe<Scalars['String']>;
  harvestDateStart?: InputMaybe<Scalars['String']>;
  harvestTonnageEstimationKg: Scalars['Float'];
  harvestType?: InputMaybe<HarvestType>;
  hasHarvestTeam?: InputMaybe<Scalars['Boolean']>;
  hasLogistic?: InputMaybe<Scalars['Boolean']>;
  hasSortingTeam?: InputMaybe<Scalars['Boolean']>;
  isInterest?: InputMaybe<Scalars['Boolean']>;
  notes?: InputMaybe<Scalars['String']>;
  pondAddress?: InputMaybe<Scalars['String']>;
  pondCityId?: InputMaybe<Scalars['Float']>;
  pondCityName?: InputMaybe<Scalars['String']>;
  pondConstruction?: InputMaybe<PondConstruction>;
  pondCount: Scalars['Float'];
  pondName?: InputMaybe<Scalars['String']>;
  pondProvinceId?: InputMaybe<Scalars['Float']>;
  pondProvinceName?: InputMaybe<Scalars['String']>;
  pondSubdistrictId?: InputMaybe<Scalars['Float']>;
  pondSubdistrictName?: InputMaybe<Scalars['String']>;
  priceOnCar?: InputMaybe<Scalars['Int']>;
  priceOnFarm: Scalars['Int'];
  priceOnFarmMax?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['String']>;
  sampling?: InputMaybe<Array<InputMaybe<SamplingListItemInput>>>;
  supplierId: Scalars['String'];
  supplyCategory?: InputMaybe<SupplyCategory>;
  supplyType: SupplyType;
  unitCommoditySize: UnitCommoditySize;
};

export type SamplingListItemInput = {
  docVisit?: InputMaybe<Scalars['Float']>;
  meanBodyWeight?: InputMaybe<Scalars['Float']>;
  notes?: InputMaybe<Scalars['String']>;
  quality?: InputMaybe<Scalars['String']>;
  tonnageEstimationKg?: InputMaybe<Scalars['Float']>;
  waterQuality?: InputMaybe<Scalars['String']>;
};

export type GetSupplyByLeadRequestInput = {
  leads: Array<InputMaybe<Scalars['String']>>;
};

export type Country = {
  code: Scalars['ID'];
  name: Scalars['String'];
  native: Scalars['String'];
  phone: Scalars['String'];
  continent: Continent;
  capital?: Maybe<Scalars['String']>;
  currency?: Maybe<Scalars['String']>;
  languages: Array<Language>;
  emoji: Scalars['String'];
  emojiU: Scalars['String'];
  states: Array<State>;
};

export type Continent = {
  code: Scalars['ID'];
  name: Scalars['String'];
  countries: Array<Country>;
};

export type Language = {
  code: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  native?: Maybe<Scalars['String']>;
  rtl: Scalars['Boolean'];
};

export type State = {
  code?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  country: Country;
};

export type StringQueryOperatorInput = {
  eq?: InputMaybe<Scalars['String']>;
  ne?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  regex?: InputMaybe<Scalars['String']>;
  glob?: InputMaybe<Scalars['String']>;
};

export type CountryFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
  currency?: InputMaybe<StringQueryOperatorInput>;
  continent?: InputMaybe<StringQueryOperatorInput>;
};

export type ContinentFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type LanguageFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type _Entity = Country | Continent | Language;

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string | ((fieldNode: FieldNode) => SelectionSetNode);
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Query: ResolverTypeWrapper<{}>;
  Mutation: ResolverTypeWrapper<{}>;
  AuditLogIndexResponse: ResolverTypeWrapper<AuditLogIndexResponse>;
  JSON: ResolverTypeWrapper<Scalars['JSON']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Search: ResolverTypeWrapper<Search>;
  AuditSearchLogsResponse: ResolverTypeWrapper<AuditSearchLogsResponse>;
  GetLogsByFilterInput: GetLogsByFilterInput;
  Log: ResolverTypeWrapper<Log>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  CanvassingArrayResponse: ResolverTypeWrapper<CanvassingArrayResponse>;
  CanvassingResponse: ResolverTypeWrapper<CanvassingResponse>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  CultivationSystem: CultivationSystem;
  HarvestType: HarvestType;
  PondConstruction: PondConstruction;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  CanvassingProduct: ResolverTypeWrapper<CanvassingProduct>;
  CommodityCategory: ResolverTypeWrapper<CommodityCategory>;
  SamplingListItem: ResolverTypeWrapper<SamplingListItem>;
  SupplyCategory: SupplyCategory;
  SupplyType: SupplyType;
  UnitCommoditySize: UnitCommoditySize;
  CanvassingSingleResponse: ResolverTypeWrapper<CanvassingSingleResponse>;
  CanvassingSummaryResponse: ResolverTypeWrapper<CanvassingSummaryResponse>;
  CanvassingSummary: ResolverTypeWrapper<CanvassingSummary>;
  TotalSupply: ResolverTypeWrapper<TotalSupply>;
  TotalValid: ResolverTypeWrapper<TotalValid>;
  CanvassingRequestInput: CanvassingRequestInput;
  SamplingListItemInput: SamplingListItemInput;
  GetSupplyByLeadRequestInput: GetSupplyByLeadRequestInput;
  Country: ResolverTypeWrapper<Country>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Continent: ResolverTypeWrapper<Continent>;
  Language: ResolverTypeWrapper<Language>;
  State: ResolverTypeWrapper<State>;
  StringQueryOperatorInput: StringQueryOperatorInput;
  CountryFilterInput: CountryFilterInput;
  ContinentFilterInput: ContinentFilterInput;
  LanguageFilterInput: LanguageFilterInput;
  _Any: ResolverTypeWrapper<Scalars['_Any']>;
  _Entity: ResolversTypes['Country'] | ResolversTypes['Continent'] | ResolversTypes['Language'];
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Query: {};
  Mutation: {};
  AuditLogIndexResponse: AuditLogIndexResponse;
  JSON: Scalars['JSON'];
  String: Scalars['String'];
  Search: Search;
  AuditSearchLogsResponse: AuditSearchLogsResponse;
  GetLogsByFilterInput: GetLogsByFilterInput;
  Log: Log;
  Boolean: Scalars['Boolean'];
  CanvassingArrayResponse: CanvassingArrayResponse;
  CanvassingResponse: CanvassingResponse;
  Float: Scalars['Float'];
  Int: Scalars['Int'];
  CanvassingProduct: CanvassingProduct;
  CommodityCategory: CommodityCategory;
  SamplingListItem: SamplingListItem;
  CanvassingSingleResponse: CanvassingSingleResponse;
  CanvassingSummaryResponse: CanvassingSummaryResponse;
  CanvassingSummary: CanvassingSummary;
  TotalSupply: TotalSupply;
  TotalValid: TotalValid;
  CanvassingRequestInput: CanvassingRequestInput;
  SamplingListItemInput: SamplingListItemInput;
  GetSupplyByLeadRequestInput: GetSupplyByLeadRequestInput;
  Country: Country;
  ID: Scalars['ID'];
  Continent: Continent;
  Language: Language;
  State: State;
  StringQueryOperatorInput: StringQueryOperatorInput;
  CountryFilterInput: CountryFilterInput;
  ContinentFilterInput: ContinentFilterInput;
  LanguageFilterInput: LanguageFilterInput;
  _Any: Scalars['_Any'];
  _Entity: ResolversParentTypes['Country'] | ResolversParentTypes['Continent'] | ResolversParentTypes['Language'];
}>;

export type QueryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  auditLogIndexResponse?: Resolver<Maybe<ResolversTypes['AuditLogIndexResponse']>, ParentType, ContextType>;
  canvassingArrayResponse?: Resolver<Maybe<ResolversTypes['CanvassingArrayResponse']>, ParentType, ContextType, Partial<QuerycanvassingArrayResponseArgs>>;
  canvassingSingleResponse?: Resolver<Maybe<ResolversTypes['CanvassingSingleResponse']>, ParentType, ContextType, RequireFields<QuerycanvassingSingleResponseArgs, 'supplyId'>>;
  canvassingSummaryResponse?: Resolver<Maybe<ResolversTypes['CanvassingSummaryResponse']>, ParentType, ContextType, Partial<QuerycanvassingSummaryResponseArgs>>;
  countries?: Resolver<Array<ResolversTypes['Country']>, ParentType, ContextType, Partial<QuerycountriesArgs>>;
  country?: Resolver<Maybe<ResolversTypes['Country']>, ParentType, ContextType, RequireFields<QuerycountryArgs, 'code'>>;
  continents?: Resolver<Array<ResolversTypes['Continent']>, ParentType, ContextType, Partial<QuerycontinentsArgs>>;
  continent?: Resolver<Maybe<ResolversTypes['Continent']>, ParentType, ContextType, RequireFields<QuerycontinentArgs, 'code'>>;
  languages?: Resolver<Array<ResolversTypes['Language']>, ParentType, ContextType, Partial<QuerylanguagesArgs>>;
  language?: Resolver<Maybe<ResolversTypes['Language']>, ParentType, ContextType, RequireFields<QuerylanguageArgs, 'code'>>;
  _entities?: Resolver<Array<Maybe<ResolversTypes['_Entity']>>, ParentType, ContextType, RequireFields<Query_entitiesArgs, 'representations'>>;
}>;

export type MutationResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  searchAuditlogs?: Resolver<Maybe<ResolversTypes['Search']>, ParentType, ContextType, RequireFields<MutationsearchAuditlogsArgs, 'getLogsByFilterInput'>>;
  storeAuditlog?: Resolver<Maybe<ResolversTypes['Log']>, ParentType, ContextType, RequireFields<MutationstoreAuditlogArgs, 'getLogsByFilterInput'>>;
  createSupply?: Resolver<Maybe<ResolversTypes['CanvassingSingleResponse']>, ParentType, ContextType, RequireFields<MutationcreateSupplyArgs, 'canvassingRequestInput'>>;
  getSupplyLead?: Resolver<Maybe<Array<Maybe<ResolversTypes['CanvassingSingleResponse']>>>, ParentType, ContextType, RequireFields<MutationgetSupplyLeadArgs, 'getSupplyByLeadRequestInput'>>;
  updateSupplyById?: Resolver<Maybe<ResolversTypes['CanvassingSingleResponse']>, ParentType, ContextType, RequireFields<MutationupdateSupplyByIdArgs, 'canvassingRequestInput' | 'supplyId'>>;
}>;

export type AuditLogIndexResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['AuditLogIndexResponse'] = ResolversParentTypes['AuditLogIndexResponse']> = ResolversObject<{
  data?: Resolver<Maybe<ResolversTypes['JSON']>, ParentType, ContextType>;
  message?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  status?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface JSONScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export type SearchResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Search'] = ResolversParentTypes['Search']> = ResolversObject<{
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['AuditSearchLogsResponse']>>>, ParentType, ContextType>;
  message?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  status?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AuditSearchLogsResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['AuditSearchLogsResponse'] = ResolversParentTypes['AuditSearchLogsResponse']> = ResolversObject<{
  activity?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  actor?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  clientIdRef?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  content?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  project?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  sysData?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  sysMessage?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  sysStatus?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  tag?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  timestamp?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LogResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Log'] = ResolversParentTypes['Log']> = ResolversObject<{
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['AuditSearchLogsResponse']>>>, ParentType, ContextType>;
  message?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  status?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CanvassingArrayResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingArrayResponse'] = ResolversParentTypes['CanvassingArrayResponse']> = ResolversObject<{
  data?: Resolver<Array<Maybe<ResolversTypes['CanvassingResponse']>>, ParentType, ContextType>;
  message?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  success?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CanvassingResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingResponse'] = ResolversParentTypes['CanvassingResponse']> = ResolversObject<{
  commoditySize?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  commoditySizeMax?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  cultivationSystem?: Resolver<Maybe<ResolversTypes['CultivationSystem']>, ParentType, ContextType>;
  feedConsumptionKg?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  feedType?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  harvestDateEnd?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  harvestDateStart?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  harvestTonnageEstimationKg?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  harvestType?: Resolver<Maybe<ResolversTypes['HarvestType']>, ParentType, ContextType>;
  hasHarvestTeam?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  hasLogistic?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  hasSortingTeam?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  isInterest?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  notes?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pondAddress?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pondCityId?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  pondCityName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pondConstruction?: Resolver<Maybe<ResolversTypes['PondConstruction']>, ParentType, ContextType>;
  pondName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pondProvinceId?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  pondProvinceName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  pondSubdistrictId?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  pondSubdistrictName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  priceOnCar?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  priceOnFarm?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  priceOnFarmMax?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  productId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  products?: Resolver<Maybe<ResolversTypes['CanvassingProduct']>, ParentType, ContextType>;
  sampling?: Resolver<Maybe<Array<Maybe<ResolversTypes['SamplingListItem']>>>, ParentType, ContextType>;
  supplierId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  supplierName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  supplierTelp?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  supplyCategory?: Resolver<Maybe<ResolversTypes['SupplyCategory']>, ParentType, ContextType>;
  supplyType?: Resolver<Maybe<ResolversTypes['SupplyType']>, ParentType, ContextType>;
  unitCommoditySize?: Resolver<Maybe<ResolversTypes['UnitCommoditySize']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CultivationSystemResolvers = { INTENSIVE: 'undefined', SEMI_INTENSIVE: 'SEMI INTENSIVE', TRADISIONAL: 'undefined' };

export type CanvassingProductResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingProduct'] = ResolversParentTypes['CanvassingProduct']> = ResolversObject<{
  commodityCategory?: Resolver<Maybe<ResolversTypes['CommodityCategory']>, ParentType, ContextType>;
  commodityCategoryId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  commoditySize?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  commoditySizeMax?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  createdAt?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  glazing?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  grade?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  isCanvassing?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  packingSize?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  productType?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  remark?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  skuName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  unitCommoditySize?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  unitInventory?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  updatedAt?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CommodityCategoryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CommodityCategory'] = ResolversParentTypes['CommodityCategory']> = ResolversObject<{
  category?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  commodityName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type SamplingListItemResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['SamplingListItem'] = ResolversParentTypes['SamplingListItem']> = ResolversObject<{
  docVisit?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  meanBodyWeight?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  notes?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  quality?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  tonnageEstimationKg?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  waterQuality?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type UnitCommoditySizeResolvers = { GRAM_EKOR: 'GRAM/EKOR', EKOR_KG: 'EKOR/KG', GRAM_PCS: 'GRAM/PCS', PCS_PAK: 'PCS/PAK' };

export type CanvassingSingleResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingSingleResponse'] = ResolversParentTypes['CanvassingSingleResponse']> = ResolversObject<{
  data?: Resolver<ResolversTypes['CanvassingResponse'], ParentType, ContextType>;
  message?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  success?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CanvassingSummaryResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingSummaryResponse'] = ResolversParentTypes['CanvassingSummaryResponse']> = ResolversObject<{
  data?: Resolver<ResolversTypes['CanvassingSummary'], ParentType, ContextType>;
  message?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  success?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CanvassingSummaryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['CanvassingSummary'] = ResolversParentTypes['CanvassingSummary']> = ResolversObject<{
  totalSupply?: Resolver<ResolversTypes['TotalSupply'], ParentType, ContextType>;
  totalValid?: Resolver<ResolversTypes['TotalValid'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type TotalSupplyResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['TotalSupply'] = ResolversParentTypes['TotalSupply']> = ResolversObject<{
  totalHarvest?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  totalPrice?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type TotalValidResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['TotalValid'] = ResolversParentTypes['TotalValid']> = ResolversObject<{
  totalHarvest?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  totalPrice?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CountryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Country'] = ResolversParentTypes['Country']> = ResolversObject<{
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  native?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  phone?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  continent?: Resolver<ResolversTypes['Continent'], ParentType, ContextType>;
  capital?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  currency?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  languages?: Resolver<Array<ResolversTypes['Language']>, ParentType, ContextType>;
  emoji?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  emojiU?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  states?: Resolver<Array<ResolversTypes['State']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ContinentResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Continent'] = ResolversParentTypes['Continent']> = ResolversObject<{
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  countries?: Resolver<Array<ResolversTypes['Country']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LanguageResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Language'] = ResolversParentTypes['Language']> = ResolversObject<{
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  native?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  rtl?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type StateResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['State'] = ResolversParentTypes['State']> = ResolversObject<{
  code?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  country?: Resolver<ResolversTypes['Country'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface _AnyScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['_Any'], any> {
  name: '_Any';
}

export type _EntityResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['_Entity'] = ResolversParentTypes['_Entity']> = ResolversObject<{
  __resolveType: TypeResolveFn<'Country' | 'Continent' | 'Language', ParentType, ContextType>;
}>;

export type Resolvers<ContextType = MeshContext> = ResolversObject<{
  Query?: QueryResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  AuditLogIndexResponse?: AuditLogIndexResponseResolvers<ContextType>;
  JSON?: GraphQLScalarType;
  Search?: SearchResolvers<ContextType>;
  AuditSearchLogsResponse?: AuditSearchLogsResponseResolvers<ContextType>;
  Log?: LogResolvers<ContextType>;
  CanvassingArrayResponse?: CanvassingArrayResponseResolvers<ContextType>;
  CanvassingResponse?: CanvassingResponseResolvers<ContextType>;
  CultivationSystem?: CultivationSystemResolvers;
  CanvassingProduct?: CanvassingProductResolvers<ContextType>;
  CommodityCategory?: CommodityCategoryResolvers<ContextType>;
  SamplingListItem?: SamplingListItemResolvers<ContextType>;
  UnitCommoditySize?: UnitCommoditySizeResolvers;
  CanvassingSingleResponse?: CanvassingSingleResponseResolvers<ContextType>;
  CanvassingSummaryResponse?: CanvassingSummaryResponseResolvers<ContextType>;
  CanvassingSummary?: CanvassingSummaryResolvers<ContextType>;
  TotalSupply?: TotalSupplyResolvers<ContextType>;
  TotalValid?: TotalValidResolvers<ContextType>;
  Country?: CountryResolvers<ContextType>;
  Continent?: ContinentResolvers<ContextType>;
  Language?: LanguageResolvers<ContextType>;
  State?: StateResolvers<ContextType>;
  _Any?: GraphQLScalarType;
  _Entity?: _EntityResolvers<ContextType>;
}>;


import { MeshContext as BaseMeshContext, MeshInstance } from '@graphql-mesh/runtime';

import { InContextSdkMethod } from '@graphql-mesh/types';


    export namespace AuditLogServiceTypes {
      export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  JSON: any;
};

export type Query = {
  /**
   * Index Page
   *
   * Equivalent to GET /
   */
  auditLogIndexResponse?: Maybe<AuditLogIndexResponse>;
};

export type AuditLogIndexResponse = {
  data?: Maybe<Scalars['JSON']>;
  message: Scalars['String'];
  status: Scalars['String'];
};

export type Mutation = {
  /**
   * Get Logs by Filter. Request body must have at least 1 key-value
   *
   * Equivalent to POST /search
   */
  searchAuditlogs?: Maybe<Search>;
  /**
   * Store audit log to database
   *
   * Equivalent to POST /log
   */
  storeAuditlog?: Maybe<Log>;
};


export type MutationsearchAuditlogsArgs = {
  getLogsByFilterInput: GetLogsByFilterInput;
};


export type MutationstoreAuditlogArgs = {
  getLogsByFilterInput: GetLogsByFilterInput;
};

export type Search = {
  data?: Maybe<Array<Maybe<AuditSearchLogsResponse>>>;
  message?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type AuditSearchLogsResponse = {
  activity?: Maybe<Scalars['String']>;
  actor?: Maybe<Scalars['String']>;
  clientIdRef?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  project?: Maybe<Scalars['String']>;
  sysData?: Maybe<Scalars['String']>;
  sysMessage?: Maybe<Scalars['String']>;
  sysStatus?: Maybe<Scalars['String']>;
  tag?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

export type GetLogsByFilterInput = {
  activity: Scalars['String'];
  actor?: InputMaybe<Scalars['String']>;
  clientIdRef: Scalars['String'];
  content: Scalars['String'];
  project: Scalars['String'];
  sysData?: InputMaybe<Scalars['String']>;
  sysMessage?: InputMaybe<Scalars['String']>;
  sysStatus?: InputMaybe<Scalars['String']>;
  tag?: InputMaybe<Scalars['String']>;
  type: Scalars['String'];
};

export type Log = {
  data?: Maybe<Array<Maybe<AuditSearchLogsResponse>>>;
  message?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

    }
    export type QueryAuditLogServiceSdk = {
  /** Index Page

Equivalent to GET / **/
  auditLogIndexResponse: InContextSdkMethod<AuditLogServiceTypes.Query['auditLogIndexResponse'], {}, MeshContext>
};

export type MutationAuditLogServiceSdk = {
  /** Get Logs by Filter. Request body must have at least 1 key-value

Equivalent to POST /search **/
  searchAuditlogs: InContextSdkMethod<AuditLogServiceTypes.Mutation['searchAuditlogs'], AuditLogServiceTypes.MutationsearchAuditlogsArgs, MeshContext>,
  /** Store audit log to database

Equivalent to POST /log **/
  storeAuditlog: InContextSdkMethod<AuditLogServiceTypes.Mutation['storeAuditlog'], AuditLogServiceTypes.MutationstoreAuditlogArgs, MeshContext>
};

export type SubscriptionAuditLogServiceSdk = {

};


    export namespace SupplyServiceTypes {
      export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  /**
   * Get supply canvassing
   *
   * Equivalent to GET /supply
   */
  canvassingArrayResponse?: Maybe<CanvassingArrayResponse>;
  /**
   * Update a new supply canvassing
   *
   * Equivalent to GET /supply/{supply_id}
   */
  canvassingSingleResponse?: Maybe<CanvassingSingleResponse>;
  /**
   * Get supply lead in supply canvassing
   *
   * Equivalent to GET /supply/summary
   */
  canvassingSummaryResponse?: Maybe<CanvassingSummaryResponse>;
};


export type QuerycanvassingArrayResponseArgs = {
  keyword?: InputMaybe<Scalars['String']>;
  limit?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QuerycanvassingSingleResponseArgs = {
  supplyId: Scalars['String'];
};


export type QuerycanvassingSummaryResponseArgs = {
  dateEnd?: InputMaybe<Scalars['String']>;
  dateStart?: InputMaybe<Scalars['String']>;
};

export type CanvassingArrayResponse = {
  data: Array<Maybe<CanvassingResponse>>;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingResponse = {
  commoditySize?: Maybe<Scalars['Float']>;
  commoditySizeMax?: Maybe<Scalars['Float']>;
  cultivationSystem?: Maybe<CultivationSystem>;
  feedConsumptionKg?: Maybe<Scalars['Float']>;
  feedType?: Maybe<Scalars['String']>;
  harvestDateEnd?: Maybe<Scalars['String']>;
  harvestDateStart?: Maybe<Scalars['String']>;
  harvestTonnageEstimationKg?: Maybe<Scalars['Float']>;
  harvestType?: Maybe<HarvestType>;
  hasHarvestTeam?: Maybe<Scalars['Boolean']>;
  hasLogistic?: Maybe<Scalars['Boolean']>;
  hasSortingTeam?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
  isInterest?: Maybe<Scalars['Boolean']>;
  notes?: Maybe<Scalars['String']>;
  pondAddress?: Maybe<Scalars['String']>;
  pondCityId?: Maybe<Scalars['Float']>;
  pondCityName?: Maybe<Scalars['String']>;
  pondConstruction?: Maybe<PondConstruction>;
  pondName?: Maybe<Scalars['String']>;
  pondProvinceId?: Maybe<Scalars['Float']>;
  pondProvinceName?: Maybe<Scalars['String']>;
  pondSubdistrictId?: Maybe<Scalars['Float']>;
  pondSubdistrictName?: Maybe<Scalars['String']>;
  priceOnCar?: Maybe<Scalars['Int']>;
  priceOnFarm?: Maybe<Scalars['Int']>;
  priceOnFarmMax?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['String']>;
  products?: Maybe<CanvassingProduct>;
  sampling?: Maybe<Array<Maybe<SamplingListItem>>>;
  supplierId?: Maybe<Scalars['String']>;
  supplierName?: Maybe<Scalars['String']>;
  supplierTelp?: Maybe<Scalars['String']>;
  supplyCategory?: Maybe<SupplyCategory>;
  supplyType?: Maybe<SupplyType>;
  unitCommoditySize?: Maybe<UnitCommoditySize>;
};

export type CultivationSystem =
  | 'INTENSIVE'
  | 'SEMI_INTENSIVE'
  | 'TRADISIONAL';

export type HarvestType =
  | 'TOTAL'
  | 'PARTIAL';

export type PondConstruction =
  | 'BETON'
  | 'MULSA'
  | 'HDPE'
  | 'TANAH'
  | 'TERPAL';

export type CanvassingProduct = {
  commodityCategory?: Maybe<CommodityCategory>;
  commodityCategoryId?: Maybe<Scalars['String']>;
  commoditySize?: Maybe<Scalars['Float']>;
  commoditySizeMax?: Maybe<Scalars['Float']>;
  createdAt?: Maybe<Scalars['String']>;
  glazing?: Maybe<Scalars['String']>;
  grade?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  isCanvassing?: Maybe<Scalars['Boolean']>;
  packingSize?: Maybe<Scalars['String']>;
  productType?: Maybe<Scalars['String']>;
  remark?: Maybe<Scalars['String']>;
  skuName?: Maybe<Scalars['String']>;
  unitCommoditySize?: Maybe<Scalars['String']>;
  unitInventory?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
};

export type CommodityCategory = {
  category?: Maybe<Scalars['String']>;
  commodityName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type SamplingListItem = {
  docVisit?: Maybe<Scalars['Float']>;
  meanBodyWeight?: Maybe<Scalars['Float']>;
  notes?: Maybe<Scalars['String']>;
  quality?: Maybe<Scalars['String']>;
  tonnageEstimationKg?: Maybe<Scalars['Float']>;
  waterQuality?: Maybe<Scalars['String']>;
};

export type SupplyCategory =
  | 'KOLAM'
  | 'BATCH_PANEN';

export type SupplyType =
  | 'FISH'
  | 'SHRIMP';

export type UnitCommoditySize =
  | 'GRAM_EKOR'
  | 'EKOR_KG'
  | 'GRAM_PCS'
  | 'PCS_PAK';

export type CanvassingSingleResponse = {
  data: CanvassingResponse;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingSummaryResponse = {
  data: CanvassingSummary;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type CanvassingSummary = {
  totalSupply: TotalSupply;
  totalValid: TotalValid;
};

export type TotalSupply = {
  totalHarvest: Scalars['Float'];
  totalPrice: Scalars['Float'];
};

export type TotalValid = {
  totalHarvest: Scalars['Float'];
  totalPrice: Scalars['Float'];
};

export type Mutation = {
  /**
   * Create a new supply canvassing
   *
   * Equivalent to POST /supply
   */
  createSupply?: Maybe<CanvassingSingleResponse>;
  /**
   * Get supply lead in supply canvassing
   *
   * Equivalent to POST /supply/lead
   */
  getSupplyLead?: Maybe<Array<Maybe<CanvassingSingleResponse>>>;
  /**
   * Update a new supply canvassing
   *
   * Equivalent to POST /supply/{supply_id}
   */
  updateSupplyById?: Maybe<CanvassingSingleResponse>;
};


export type MutationcreateSupplyArgs = {
  canvassingRequestInput: CanvassingRequestInput;
};


export type MutationgetSupplyLeadArgs = {
  getSupplyByLeadRequestInput: GetSupplyByLeadRequestInput;
  limit?: InputMaybe<Scalars['Int']>;
};


export type MutationupdateSupplyByIdArgs = {
  canvassingRequestInput: CanvassingRequestInput;
  supplyId: Scalars['String'];
};

export type CanvassingRequestInput = {
  commoditySize: Scalars['Float'];
  commoditySizeMax?: InputMaybe<Scalars['Float']>;
  cultivationSystem?: InputMaybe<CultivationSystem>;
  feedConsumptionKg: Scalars['Float'];
  feedType?: InputMaybe<Scalars['String']>;
  harvestDateEnd?: InputMaybe<Scalars['String']>;
  harvestDateStart?: InputMaybe<Scalars['String']>;
  harvestTonnageEstimationKg: Scalars['Float'];
  harvestType?: InputMaybe<HarvestType>;
  hasHarvestTeam?: InputMaybe<Scalars['Boolean']>;
  hasLogistic?: InputMaybe<Scalars['Boolean']>;
  hasSortingTeam?: InputMaybe<Scalars['Boolean']>;
  isInterest?: InputMaybe<Scalars['Boolean']>;
  notes?: InputMaybe<Scalars['String']>;
  pondAddress?: InputMaybe<Scalars['String']>;
  pondCityId?: InputMaybe<Scalars['Float']>;
  pondCityName?: InputMaybe<Scalars['String']>;
  pondConstruction?: InputMaybe<PondConstruction>;
  pondCount: Scalars['Float'];
  pondName?: InputMaybe<Scalars['String']>;
  pondProvinceId?: InputMaybe<Scalars['Float']>;
  pondProvinceName?: InputMaybe<Scalars['String']>;
  pondSubdistrictId?: InputMaybe<Scalars['Float']>;
  pondSubdistrictName?: InputMaybe<Scalars['String']>;
  priceOnCar?: InputMaybe<Scalars['Int']>;
  priceOnFarm: Scalars['Int'];
  priceOnFarmMax?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['String']>;
  sampling?: InputMaybe<Array<InputMaybe<SamplingListItemInput>>>;
  supplierId: Scalars['String'];
  supplyCategory?: InputMaybe<SupplyCategory>;
  supplyType: SupplyType;
  unitCommoditySize: UnitCommoditySize;
};

export type SamplingListItemInput = {
  docVisit?: InputMaybe<Scalars['Float']>;
  meanBodyWeight?: InputMaybe<Scalars['Float']>;
  notes?: InputMaybe<Scalars['String']>;
  quality?: InputMaybe<Scalars['String']>;
  tonnageEstimationKg?: InputMaybe<Scalars['Float']>;
  waterQuality?: InputMaybe<Scalars['String']>;
};

export type GetSupplyByLeadRequestInput = {
  leads: Array<InputMaybe<Scalars['String']>>;
};

    }
    export type QuerySupplyServiceSdk = {
  /** Get supply canvassing

Equivalent to GET /supply **/
  canvassingArrayResponse: InContextSdkMethod<SupplyServiceTypes.Query['canvassingArrayResponse'], SupplyServiceTypes.QuerycanvassingArrayResponseArgs, MeshContext>,
  /** Update a new supply canvassing

Equivalent to GET /supply/{supply_id} **/
  canvassingSingleResponse: InContextSdkMethod<SupplyServiceTypes.Query['canvassingSingleResponse'], SupplyServiceTypes.QuerycanvassingSingleResponseArgs, MeshContext>,
  /** Get supply lead in supply canvassing

Equivalent to GET /supply/summary **/
  canvassingSummaryResponse: InContextSdkMethod<SupplyServiceTypes.Query['canvassingSummaryResponse'], SupplyServiceTypes.QuerycanvassingSummaryResponseArgs, MeshContext>
};

export type MutationSupplyServiceSdk = {
  /** Create a new supply canvassing

Equivalent to POST /supply **/
  createSupply: InContextSdkMethod<SupplyServiceTypes.Mutation['createSupply'], SupplyServiceTypes.MutationcreateSupplyArgs, MeshContext>,
  /** Get supply lead in supply canvassing

Equivalent to POST /supply/lead **/
  getSupplyLead: InContextSdkMethod<SupplyServiceTypes.Mutation['getSupplyLead'], SupplyServiceTypes.MutationgetSupplyLeadArgs, MeshContext>,
  /** Update a new supply canvassing

Equivalent to POST /supply/{supply_id} **/
  updateSupplyById: InContextSdkMethod<SupplyServiceTypes.Mutation['updateSupplyById'], SupplyServiceTypes.MutationupdateSupplyByIdArgs, MeshContext>
};

export type SubscriptionSupplyServiceSdk = {

};


    export namespace CountriesGraphQlTypes {
      export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  _Any: any;
};

export type Country = {
  code: Scalars['ID'];
  name: Scalars['String'];
  native: Scalars['String'];
  phone: Scalars['String'];
  continent: Continent;
  capital?: Maybe<Scalars['String']>;
  currency?: Maybe<Scalars['String']>;
  languages: Array<Language>;
  emoji: Scalars['String'];
  emojiU: Scalars['String'];
  states: Array<State>;
};

export type Continent = {
  code: Scalars['ID'];
  name: Scalars['String'];
  countries: Array<Country>;
};

export type Language = {
  code: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  native?: Maybe<Scalars['String']>;
  rtl: Scalars['Boolean'];
};

export type State = {
  code?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  country: Country;
};

export type StringQueryOperatorInput = {
  eq?: InputMaybe<Scalars['String']>;
  ne?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  nin?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  regex?: InputMaybe<Scalars['String']>;
  glob?: InputMaybe<Scalars['String']>;
};

export type CountryFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
  currency?: InputMaybe<StringQueryOperatorInput>;
  continent?: InputMaybe<StringQueryOperatorInput>;
};

export type ContinentFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type LanguageFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type Query = {
  countries: Array<Country>;
  country?: Maybe<Country>;
  continents: Array<Continent>;
  continent?: Maybe<Continent>;
  languages: Array<Language>;
  language?: Maybe<Language>;
  _entities: Array<Maybe<_Entity>>;
};


export type QuerycountriesArgs = {
  filter?: InputMaybe<CountryFilterInput>;
};


export type QuerycountryArgs = {
  code: Scalars['ID'];
};


export type QuerycontinentsArgs = {
  filter?: InputMaybe<ContinentFilterInput>;
};


export type QuerycontinentArgs = {
  code: Scalars['ID'];
};


export type QuerylanguagesArgs = {
  filter?: InputMaybe<LanguageFilterInput>;
};


export type QuerylanguageArgs = {
  code: Scalars['ID'];
};


export type Query_entitiesArgs = {
  representations: Array<Scalars['_Any']>;
};

export type _Entity = Country | Continent | Language;

    }
    export type QueryCountriesGraphQlSdk = {
  /** undefined **/
  countries: InContextSdkMethod<CountriesGraphQlTypes.Query['countries'], CountriesGraphQlTypes.QuerycountriesArgs, MeshContext>,
  /** undefined **/
  country: InContextSdkMethod<CountriesGraphQlTypes.Query['country'], CountriesGraphQlTypes.QuerycountryArgs, MeshContext>,
  /** undefined **/
  continents: InContextSdkMethod<CountriesGraphQlTypes.Query['continents'], CountriesGraphQlTypes.QuerycontinentsArgs, MeshContext>,
  /** undefined **/
  continent: InContextSdkMethod<CountriesGraphQlTypes.Query['continent'], CountriesGraphQlTypes.QuerycontinentArgs, MeshContext>,
  /** undefined **/
  languages: InContextSdkMethod<CountriesGraphQlTypes.Query['languages'], CountriesGraphQlTypes.QuerylanguagesArgs, MeshContext>,
  /** undefined **/
  language: InContextSdkMethod<CountriesGraphQlTypes.Query['language'], CountriesGraphQlTypes.QuerylanguageArgs, MeshContext>,
  /** undefined **/
  _entities: InContextSdkMethod<CountriesGraphQlTypes.Query['_entities'], CountriesGraphQlTypes.Query_entitiesArgs, MeshContext>
};

export type MutationCountriesGraphQlSdk = {

};

export type SubscriptionCountriesGraphQlSdk = {

};

export type AuditLogServiceContext = {
      ["Audit Log Service"]: { Query: QueryAuditLogServiceSdk, Mutation: MutationAuditLogServiceSdk, Subscription: SubscriptionAuditLogServiceSdk },
    };

export type SupplyServiceContext = {
      ["Supply Service"]: { Query: QuerySupplyServiceSdk, Mutation: MutationSupplyServiceSdk, Subscription: SubscriptionSupplyServiceSdk },
    };

export type CountriesGraphQlContext = {
      ["Countries GraphQL"]: { Query: QueryCountriesGraphQlSdk, Mutation: MutationCountriesGraphQlSdk, Subscription: SubscriptionCountriesGraphQlSdk },
    };

export type MeshContext = AuditLogServiceContext & SupplyServiceContext & CountriesGraphQlContext & BaseMeshContext;


import { getMesh } from '@graphql-mesh/runtime';
import { MeshStore, FsStoreStorageAdapter } from '@graphql-mesh/store';
import { path as pathModule } from '@graphql-mesh/cross-helpers';
import { fileURLToPath } from '@graphql-mesh/utils';

const importedModules: Record<string, any> = {

};

const baseDir = pathModule.join(__dirname, '..');

const importFn = (moduleId: string) => {
  const relativeModuleId = (pathModule.isAbsolute(moduleId) ? pathModule.relative(baseDir, moduleId) : moduleId).split('\\').join('/').replace(baseDir + '/', '');
  if (!(relativeModuleId in importedModules)) {
    throw new Error(`Cannot find module '${relativeModuleId}'.`);
  }
  return Promise.resolve(importedModules[relativeModuleId]);
};

const rootStore = new MeshStore('.mesh', new FsStoreStorageAdapter({
  cwd: baseDir,
  importFn,
}), {
  readonly: true,
  validate: false
});


                import { findAndParseConfig } from '@graphql-mesh/cli';
                function getMeshOptions() {
                  console.warn('WARNING: These artifacts are built for development mode. Please run "mesh build" to build production artifacts');
                  return findAndParseConfig({
                    dir: baseDir,
                    artifactsDir: ".mesh",
                    configName: "mesh",
                  });
                }
              

export const documentsInSDL = /*#__PURE__*/ [];

export async function getBuiltMesh(): Promise<MeshInstance<MeshContext>> {
  const meshConfig = await getMeshOptions();
  return getMesh<MeshContext>(meshConfig);
}

export async function getMeshSDK<TGlobalContext = any, TOperationContext = any>(globalContext?: TGlobalContext) {
  const { sdkRequesterFactory } = await getBuiltMesh();
  return getSdk<TOperationContext>(sdkRequesterFactory(globalContext));
}

export type Requester<C= {}> = <R, V>(doc: DocumentNode, vars?: V, options?: C) => Promise<R>
export function getSdk<C>(requester: Requester<C>) {
  return {

  };
}
export type Sdk = ReturnType<typeof getSdk>;